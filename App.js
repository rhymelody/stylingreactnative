import React from 'react';
import {
  SafeAreaView,
  StatusBar,
  Image,
  Text,
  useColorScheme,
  View
} from 'react-native';

import Style from './style';

const App = () => {

  const isDarkMode = useColorScheme() === 'light';

  const backgroundStyle = {
    backgroundColor: isDarkMode ? 'black' : 'white',
  };

  return (
    <SafeAreaView style={backgroundStyle}>
      <StatusBar barStyle={isDarkMode ? 'light-content' : 'dark-content'} />
      <View style={Style.container}>
        <View style={Style.card}>
          <Image 
            style={Style.image}
            resizeMode="cover"
            source={{uri:"https://res.cloudinary.com/djyjm9ayd/image/upload/v1643249117/19025216_1439860629412992_3671167199250762358_o_hzryz8.png"}}
          />
          <View style={Style.content}>
            <Text style={Style.content.header}>Styling di React Native</Text>
            <Text style={Style.content.description}>Binar Academy - React Native</Text>
            <Text style={Style.content.body}>
              As a component grows in complexity, it is much cleaner and efficient to use StyleSheet create so as to define several styles in one place.
            </Text>
            <View style={Style.content.footer}>
              <Text style={Style.content.footer.text}>Understood!</Text>
              <Text style={Style.content.footer.text}>What??!</Text>
            </View>
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
};

export default App;
