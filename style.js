import EStyleSheet from 'react-native-extended-stylesheet';
import {Dimensions} from 'react-native';

const screen = Dimensions.get("screen");

export default Style = EStyleSheet.create({
    container: {
        height: '100%', 
        padding: '1.5rem',
        backgroundColor: 'white'
    },
    card: {
        width: '100%',
        borderRadius: '.5rem',
        backgroundColor : 'white',
        shadowColor: 'black',
        shadowOffset: {width: 0, height: '.125rem'},
        shadowOpacity: 0.25,
        shadowRadius: '.5rem'
    },
    image: {
        height: screen.width * 0.825,
        borderTopLeftRadius: '.5rem',
        borderTopRightRadius: '.5rem'
    },
    content: {
        padding: '1rem', 
        paddingBottom: '2.5rem', 
        alignItems: 'center',
        header: {
            fontSize: '1.25rem'
        },
        description: {
            color: '#888', 
            marginTop: '.25rem'
        },
        body: {
            fontSize: '.825rem', 
            marginTop: '1rem'
        },
        footer: {
            width: '100%', 
            marginTop: '1rem', 
            flexDirection: 'row', 
            justifyContent: 'space-around',
            text: {
                color: 'darkgreen', 
                fontSize: '1.125rem'
            }
        }
    }
});

EStyleSheet.build();